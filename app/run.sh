#!/bin/bash
if [[ -z $1 ]]; then 
    echo "Specify port plz"
    exit
fi
echo 'Check env if FLAG exists'
env | grep FLAG

if [[ "$(whoami)" =~ $BIN_NAME ]]; then
    echo "Working in docker, check if binary exists"
    test -f "./$BIN_NAME"
    if [[ $? -eq 0 ]]; then
        echo "$FLAG" > /home/$BIN_NAME/flag.txt
        echo "flag.txt created"
    fi
else
    echo "Working in host, compiling with no defense..."
    gcc -w -m32 -no-pie -fno-stack-protector -z execstack -o $BIN_NAME ./root.c 
fi

if [[ $? -eq 0 ]]; then
    echo "File is ok. start serving localhost:$1"

    socat -s -v tcp-l:$1,reuseaddr,fork "system:./$BIN_NAME 2>&1"  # ,pty,echo=0

    echo "\nExited after serving on port $1"
else
    echo "Failed compiler or file does not exists"
fi
