//#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct user {
  int isAdmin;
  char *name;
};

void setup()
{
        setvbuf(stdout, 0, 2, 0);
}

void winner() {
    FILE *fp;
    char flag[30];
    fp = fopen("flag.txt", "r");
    if (!fp) return 1;
    fgets(flag, 30, fp);
    printf("%s", flag);
    fclose(fp);
}

int main(int argc, char **argv) {
  setup();
  struct user *u1, *u2;
  char * buf;

  printf("Creating user for system...\n");
  u1 = malloc(sizeof(struct user));
  u1->isAdmin = 0;
  u1->name = malloc(8);

  u2 = malloc(sizeof(struct user));
  u2->isAdmin = 1;
  u2->name = malloc(8);
  printf("Completed!\n");

  printf("Now pick name for current user:\n");
  gets(buf);
  strcpy(u1->name, buf);

  printf("Ok, %s\nWould you like to change root user's name? (1/0)\n", u1->name);
  signed int answer = 0;
  scanf("%1d", &answer);

  if (answer) {
    printf("Name:\n");
    scanf("%s",buf);
    strcpy(u2->name, buf);
  }

  printf("[debug] %p: %d\n", &u1->isAdmin, u1->isAdmin);
  if (u1->isAdmin == 1) {
      printf(  "%s",  "Log in as root user, check your brand new nickname!\n");
      winner(); 
  } else{
 	printf("\nThat's all for now...\n"); 
  }
  printf("exit...\n"); 
}
