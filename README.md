# Toor
Hey, I'm still learning C! Could you check my admin panel if there is something wrong..?

[nc mctf.ru 2222]

# Difficulty
Hard

# Type
PWN

# Pre-start

`2222` - NCAT FOR BINARY

Binary: **./app/toor**

But, there's compilation script **./run.sh** which contains gcc command to build

# Launch

    # docker-compose build
    # docker-compose up

# Solve
В бинарном файле дважды создавались структуры пользователей со след. полями: флаг админа и имя пользователя.
Затем можно было выбрать имя для двух пользователей. Имя можно было переполнить, поэтому мы имеем возможность перезаписать указатель на место куда мы записываем имя второго пользака. Итого: 20 символов и адрес записи в флаг первого пользователя дают "нашему" пользователю права админа и выводят нам флаг. Вариант пейлоада:

    $ cat payload.txt | nc localhost 2222

Или же решение при помощи *pwntools*:

    $ python3 exploit.py

# Flag

# P.S.
nothing
